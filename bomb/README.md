bomb16

## Phase_2

Read string sentence `str` from input and then pass `str` to function `phase_2(char*)`

In function phase_2, it first allocate 10 ints in stack (`sub $0x28, %rsp`) and save current the %rsp to %rsi, which is the second argument for `read_six_number`, the first argument keep the same.
Then call `read_six_numbers`, which reads six numbers to the array declared in `phase_2` from the input string.

`read_six_number` prepare six int (address) arguments for sscanf call, remember that current %rsi is the start address of int array. After calling `sscanf`, %eax is set by I/O(maybe) and contains the number of int it reads.

Then back to `phase_2(char*)`, it first checks the first element read is exactly `1`,
```
0x0000000000400f0a <+14>:    cmpl   $0x1,(%rsp)
```
then check if each value is two times of its previous value.


```
void phase_2(char* str){
int arr[10];
read_six_numbers(str, arr);
if(arr[0] != 1) "boom"
for(int i = 1; i != 6; i++){
if(arr[i] != 2 * arr[i-1]) "boom";
}
}

read_six_numbers(char* str, int* arr){

}
```

## phase_3

function `phase_3` first reads two integer from input string, save in $rcx and $rdx, then make sure it reads more that one values,
```
0x0000000000400f47 <+4>:     lea    0xc(%rsp),%rcx
0x0000000000400f4c <+9>:     lea    0x8(%rsp),%rdx
0x0000000000400f51 <+14>:    mov    $0x4025cf,%esi
0x0000000000400f56 <+19>:    mov    $0x0,%eax
=> 0x0000000000400f5b <+24>:    callq  0x400bf0 <__isoc99_sscanf@plt>
```

then check if the firt value read is less than 7, (a table jump here)
```
0x0000000000400f6a <+39>:    cmpl   $0x7,0x8(%rsp)
0x0000000000400f6f <+44>:    ja     0x400fad <phase_3+106>
0x0000000000400f71 <+46>:    mov    0x8(%rsp),%eax
```

then check if the second value read equals to 0x137, which is 311 in decimal.
```
=> 0x0000000000400fb9 <+118>:   mov    $0x137,%eax
0x0000000000400fbe <+123>:   cmp    0xc(%rsp),%eax
0x0000000000400fc2 <+127>:   je     0x400fc9 <phase_3+134>
0x0000000000400fc4 <+129>:   callq  0x40143a <explode_bomb>
0x0000000000400fc9 <+134>:   add    $0x18,%rsp
```

```
void phase_3(char* str){
int arr[6];
sscanf(str, "%d %d", &arr[2], &arr[3]);
if(arr[2] >= 7) "boom"
if(arr[3] != 311) "boom"
}
```


## phase_4

read two values from input string, boom if # read is not equal to 2.
check the first value it read is less or equal than `0xe=14`.

then call `func4` with first input, we need to make sure %eax is zero after `func4` called, that says the fist input should be equal to 7.
```
0x0000000000401048 <+60>:    callq  0x400fce <func4>
0x000000000040104d <+65>:    test   %eax,%eax
0x000000000040104f <+67>:    jne    0x401058 <phase_4+76>
0x0000000000401051 <+69>:    cmpl   $0x0,0xc(%rsp)
0x0000000000401056 <+74>:    je     0x40105d <phase_4+81>
=> 0x0000000000401058 <+76>:    callq  0x40143a <explode_bomb>
0x000000000040105d <+81>:    add    $0x18,%rsp
```

then check the second input value is 0.

```
void phase_4(char* str){
int arr[6];
int r = sscanf(str, "%d %d", &arr[2], &arr[3]);
if(r != 2) "boom"
if(arr[2] <= 14) "boom"
func4() // check arr[2] is equal to 7
if(arr[3] != 0) "boom"
}
```

## phase_5

make sure the input string length is 6,

string at `0x4024b0`(line +55) =
`maduiersnfotvbylSo you think you can stop the bomb with ctrl-c, do you?`

line 41-74 is a loop,

fetch a character from input each time, then only keep lower byte, use it as map index in `ox4024b0`.

after this loop, rsp+0x10 -> rsp+0x15 is the mapped string

`0x40245e`(+81) contains string `flyers`

then call `strings_not_qual` on the mapped string and `flyers`

so we need a string whose mapped string is `flyers`, that is `ionefg`

```
0x000000000040108b <+41>:    movzbl (%rbx,%rax,1),%ecx
0x000000000040108f <+45>:    mov    %cl,(%rsp)
0x0000000000401092 <+48>:    mov    (%rsp),%rdx
=> 0x0000000000401096 <+52>:    and    $0xf,%edx
0x0000000000401099 <+55>:    movzbl 0x4024b0(%rdx),%edx
0x00000000004010a0 <+62>:    mov    %dl,0x10(%rsp,%rax,1)
0x00000000004010a4 <+66>:    add    $0x1,%rax
0x00000000004010a8 <+70>:    cmp    $0x6,%rax
0x00000000004010ac <+74>:    jne    0x40108b <phase_5+41>
```

then check the string is exactly `aduier`
```
0x00000000004010b8 <+86>:    lea    0x10(%rsp),%rdi
=> 0x00000000004010bd <+91>:    callq  0x401338 <strings_not_equal>
0x00000000004010c2 <+96>:    test   %eax,%eax
0x00000000004010c4 <+98>:    je     0x4010d9 <phase_5+119>
0x00000000004010c6 <+100>:   callq  0x40143a <explode_bomb>
0x00000000004010cb <+105>:   nopl   0x0(%rax,%rax,1)
0x00000000004010d0 <+110>:   jmp    0x4010d9 <phase_5+119>
0x00000000004010d2 <+112>:   mov    $0x0,%eax
0x00000000004010d7 <+117>:   jmp    0x40108b <phase_5+41>
0x00000000004010d9 <+119>:   mov    0x18(%rsp),%rax
0x00000000004010de <+124>:   xor    %fs:0x28,%rax
0x00000000004010e7 <+133>:   je     0x4010ee <phase_5+140>
0x00000000004010e9 <+135>:   callq  0x400b30 <__stack_chk_fail@plt>
0x00000000004010ee <+140>:   add    $0x20,%rsp
0x00000000004010f2 <+144>:   pop    %rbx
0x00000000004010f3 <+145>:   retq
End of assembler dump.
(gdb) print $rdi
$10 = 140737488347648
(gdb) x/s $rdi
0x7fffffffe200: "aduier"
(gdb)
```

##

read six numbers

sub 1 from the first number, then compare with 5, it should <= 5, so first number <= 6


+32 -> +93 is two loop to make sure all input values are identical.
```
0x0000000000401114 <+32>:    mov    %r13,%rbp
0x0000000000401117 <+35>:    mov    0x0(%r13),%eax
0x000000000040111b <+39>:    sub    $0x1,%eax
0x000000000040111e <+42>:    cmp    $0x5,%eax
0x0000000000401121 <+45>:    jbe    0x401128 <phase_6+52>
0x0000000000401123 <+47>:    callq  0x40143a <explode_bomb>
0x0000000000401128 <+52>:    add    $0x1,%r12d
0x000000000040112c <+56>:    cmp    $0x6,%r12d
0x0000000000401130 <+60>:    je     0x401153 <phase_6+95>
0x0000000000401132 <+62>:    mov    %r12d,%ebx
0x0000000000401135 <+65>:    movslq %ebx,%rax
0x0000000000401138 <+68>:    mov    (%rsp,%rax,4),%eax
0x000000000040113b <+71>:    cmp    %eax,0x0(%rbp)
0x000000000040113e <+74>:    jne    0x401145 <phase_6+81>
0x0000000000401140 <+76>:    callq  0x40143a <explode_bomb>
0x0000000000401145 <+81>:    add    $0x1,%ebx
0x0000000000401148 <+84>:    cmp    $0x5,%ebx
0x000000000040114b <+87>:    jle    0x401135 <phase_6+65>
0x000000000040114d <+89>:    add    $0x4,%r13
0x0000000000401151 <+93>:    jmp    0x401114 <phase_6+32>
```
+52 -> +87 is a inner loop.
```
0x0000000000401128 <+52>:    add    $0x1,%r12d
0x000000000040112c <+56>:    cmp    $0x6,%r12d
0x0000000000401130 <+60>:    je     0x401153 <phase_6+95>
0x0000000000401132 <+62>:    mov    %r12d,%ebx
0x0000000000401135 <+65>:    movslq %ebx,%rax
0x0000000000401138 <+68>:    mov    (%rsp,%rax,4),%eax
0x000000000040113b <+71>:    cmp    %eax,0x0(%rbp)
0x000000000040113e <+74>:    jne    0x401145 <phase_6+81>
0x0000000000401140 <+76>:    callq  0x40143a <explode_bomb>
0x0000000000401145 <+81>:    add    $0x1,%ebx
0x0000000000401148 <+84>:    cmp    $0x5,%ebx
0x000000000040114b <+87>:    jle    0x401135 <phase_6+65>
```

+108 -> +121 is another loop to each input value from 7 and update the value with result.
```
0x0000000000401160 <+108>:   mov    %ecx,%edx
0x0000000000401162 <+110>:   sub    (%rax),%edx
0x0000000000401164 <+112>:   mov    %edx,(%rax)
0x0000000000401166 <+114>:   add    $0x4,%rax
0x000000000040116a <+118>:   cmp    %rsi,%rax
0x000000000040116d <+121>:   jne    0x401160 <phase_6+108>
```
for (v, i) in results, it store values using mapping,
[1=>322, 2=>168, 3=>924, 4=>691, 5=>477, 6=>443], the mapped value **pointer**
are stored at stack[20+i*8].

that's is:
```
for i in [0-5], rsp+(0x20+i*4*2) = map(vi)
```

at last, it checks the previous value is greater that next value.

after sorting the mapped value, we got
```
924=>3=>4
691=>4=>3
477=>5=>2
443=>6=>1
322=>1=>6
168=>2=>5
```
