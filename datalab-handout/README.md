
`sudo apt-get install libc6-dev-i386` if `fatal error: sys/cdefs.h: No such file or directory`

## bitMatch
`x ^ y = x & ~ y | ~ x & y = ~ (x & ~ y) & ~ ( ~x & y )`

## copyLSB
- `x ^ (x >> 1 << 1) == 0x00 => 0x0`
- `x ^ (x >> 1 << 1) == 0x01 => 0xF`

## allOddBits
a = 0xAAAAAAAA;
x = a & x;
a ^ x == 0 (0x00000000) => 1
a ^ x != 0 (0x00001000) => 0

## conditional
x, y z
mask y if x => ((~!!x)+1) & y
mask z if x => ((~!x)+1) & z

## distinctNegation
mind x = TMIN

## dividePower2
https://stackoverflow.com/questions/5061093/dividing-by-power-of-2-using-bit-shifting

Set lower k bit:
1. `1 << n - 1`
2. `1 << n + ~0`

## fitsBits
Construct the n bit x, then it should be equea to x
x = 000001111h


## isGreater
~(OF ^ SF) & ~ ZF

## trueThreeFourths

```
Suppose x = d...ddd
x =  d...d00 => a
    +0...0d0 => b
    +0...00d => c

interger part = (a+b)/2 + a/4 = 3a/4 + b/2
frac part = (2c + (b+c)) / 4 = b/4 + 3c/4
integer part + frac part = 3(a+b+c)/4
then adjust by sign
```

## biParity
using XOR operation
