/*
 * Code for basic C skills diagnostic.
 * Developed for courses 15-213/18-213/15-513 by R. E. Bryant, 2017
 */

/*
 * This program implements a queue supporting both FIFO and LIFO
 * operations.
 *
 * It uses a singly-linked list to represent the set of queue elements
 */

#include <stdlib.h>
#include <stdio.h>

#include "harness.h"
#include "queue.h"

/*
  Create empty queue.
  Return NULL if could not allocate space.
*/
queue_t *q_new()
{
    queue_t *q =  (queue_t*) malloc(sizeof(queue_t));
    /* What if malloc returned NULL? */
    if(q == NULL)
    {
        return NULL;
    }
    q->head = NULL;
    q->tail = NULL;
    q->size = 0;
    return q;
}

/* Free all storage used by queue */
void q_free(queue_t *q)
{
    /* How about freeing the list elements? */
    list_ele_t *pele = q == NULL ? NULL: q->head;
    list_ele_t *tmp;
    while(pele != NULL)
    {
        tmp = pele->next;
        free(pele);
        pele = tmp;
    }
    /* Free queue structure */
    if(q != NULL) free(q);
}

/*
  Attempt to insert element at head of queue.
  Return true if successful.
  Return false if q is NULL or could not allocate space.
 */
bool q_insert_head(queue_t *q, int v)
{
    list_ele_t *newh;
    /* What should you do if the q is NULL? */
    if(q == NULL)
    {
        return false;
    }
    newh = (list_ele_t*)malloc(sizeof(list_ele_t));
    /* What if malloc returned NULL? */
    if(newh == NULL)
    {
        return false;
    }
    newh->value = v;
    newh->next = q->head;
    q->head = newh;
    if(q->tail == NULL) q->tail = newh;
    q->size ++;
    return true;
}


/*
  Attempt to insert element at tail of queue.
  Return true if successful.
  Return false if q is NULL or could not allocate space.
 */
bool q_insert_tail(queue_t *q, int v)
{
    /* You need to write the complete code for this function */
    /* Remember: It should operate in O(1) time */
    if(q == NULL)
    {
        return false;
    }
    list_ele_t *newt = (list_ele_t*) malloc(sizeof(list_ele_t));
    if(newt == NULL)
    {
        return false;
    }
    newt->value = v;
    newt->next = NULL;
    if(q->tail == NULL)
    {
        q->head = q->tail = newt;
    }
    else
    {
        q->tail->next = newt;
        q->tail = newt;
    }
    q->size ++;
    return true;
}

/*
  Attempt to remove element from head of queue.
  Return true if successful.
  Return false if queue is NULL or empty.
  If vp non-NULL and element removed, store removed value at *vp.
  Any unused storage should be freed
*/
bool q_remove_head(queue_t *q, int *vp)
{
    /* You need to fix up this code. */
    if(q == NULL)
    {
        return false;
    }
    if(q->head == NULL)
    {
        return false;
    }
    if(vp == NULL)
    {
        return false;
    }
    list_ele_t *p = q->head;
    q->head = q->head->next;
    *vp = p->value;
    free(p);
    if(q->head == NULL) q->tail = NULL;
    q->size --;
    return true;
}

int q_size(queue_t *q)
{
    /* You need to write the code for this function */
    /* Remember: It should operate in O(1) time */
    if(q == NULL) return 0;
    return q->size;
}

/*
  Reverse elements in queue
 */
void q_reverse(queue_t *q)
{
    /* You need to write the code for this function */
    if(q == NULL)
    {
        return;
    }
    if(q->head == NULL) return;
    list_ele_t *p1 = q->head, *p2;
    q->head = NULL;
    q->tail = p1;
    while(p1->next != NULL)
    {
        p2 = p1->next;
        p1->next = q->head;
        q->head = p1;
        p1 = p2;
    }
    p1->next = q->head;
    q->head = p1;
}
