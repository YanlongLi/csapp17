
Note
====

1. Add `tail` pointer and `size` value to queue_t
2. Remember to test NULL if the parameter is a pointer. Like `vp` in `bool q_remove_head(queue_t *q, int *vp)`
3. Use the return value to indicate `SUCCESS`and `FAIL`, like `queue_t *q_new()`
